webpackJsonp([21],{

/***/ 1415:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1719)
/* template */
var __vue_template__ = __webpack_require__(1738)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/courses/CoursesDetail.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-42209f83", Component.options)
  } else {
    hotAPI.reload("data-v-42209f83", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1527:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ({
  "courses": [{
    "image": "/static/img/course1.jpg",
    "type": "management",
    "popular": "top",
    "bestseller": true,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Joanna Brew",
    "oldPrice": "900",
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 5,
    "level": "All",
    "disount": 50,
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course2.jpg",
    "type": "management",
    "popular": "top",
    "bestseller": true,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/watch?v=rbTVvpHF4cU",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "John Smith Brown",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 5,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course3.jpg",
    "type": "management",
    "popular": "top",
    "bestseller": true,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Joanna Brew",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 5,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course4.jpg",
    "type": "management",
    "popular": "top",
    "bestseller": true,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Savanna South",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course5.jpg",
    "type": "management",
    "popular": "top",
    "bestseller": true,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/watch?v=LywZELVl_10",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Arthur Parker",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course3.jpg",
    "type": "design",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "John Smith Brown",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course1.jpg",
    "type": "design",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Savanna South",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course2.jpg",
    "type": "design",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/watch?v=DKN_vgJeOO8",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Arthur Parker",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "rating": 4,
    "hours": "11.5",
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course5.jpg",
    "type": "develop",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Savanna South",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "rating": 4,
    "hours": "11.5",
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course4.jpg",
    "type": "develop",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/watch?v=-4rLLoNkLEU",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Arthur Parker",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course5.jpg",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "https://www.youtube.com/watch?v=rVM3kqHGVrk",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Joanna Brew",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course3.jpg",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "John Smith Brown,Savanna South",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course4.jpg",
    "popular": "new",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Arthur Parker",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course5.jpg",
    "popular": "new",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "John Smith Brown",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course2.jpg",
    "popular": "trending",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Savanna South",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course3.jpg",
    "popular": "trending",
    "bestseller": false,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/watch?v=Y4EOOb276gY",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Arthur Parker",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course4.jpg",
    "popular": "trending",
    "bestseller": false,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/watch?v=zWtncr2UcbA",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Joanna Brew",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }],
  "instructors": [{
    "image": "/static/avatars/user-6.jpg",
    "profile": "C++",
    "place": "Iron Network",
    "name": "John Smith Brown",
    "courses": "5",
    "number": "36247",
    "type": "students"
  }, {
    "image": "/static/avatars/user-13.jpg",
    "profile": "Marketing",
    "place": "Iron Network",
    "name": "Jonathan Red",
    "courses": "11",
    "number": "7464",
    "type": "students"
  }, {
    "image": "/static/avatars/user-8.jpg",
    "profile": "Designer",
    "place": "Iron Network",
    "name": "Arthur Parker",
    "courses": "3",
    "number": "36",
    "type": "students"
  }, {
    "image": "/static/avatars/user-35.jpg",
    "profile": "Python",
    "place": "Iron Network",
    "name": "Joanna Brew",
    "courses": "15",
    "number": "150",
    "type": "students"
  }, {
    "image": "/static/avatars/user-31.jpg",
    "profile": "Angular",
    "place": "Iron Network",
    "name": "Savanna South",
    "courses": "19",
    "number": "45793",
    "type": "students"
  }],
  "courseDetail": {
    "heading": "Quisque hendrerit ex quis dui eleifend, dapibus vehicula lacus semper.",
    "content": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
    "bestSeller": "2367532",
    "createdBy": "Sahil Goyal, Girija Bansal by James Brown",
    "lastUpdates": "11/2018",
    "language": "English",
    "demoVideoUrl": "https://www.youtube.com/watch?v=GKHTSJT9kdM",
    "learn": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vestibulum at mauris semper, tristique nulla et, tristique nulla.", "Proin sit amet ex in quam vehicula maximus sit amet vel mauris.", "Proin sit amet ex in quam vehicula maximus sit amet vel mauris", "Quisque hendrerit ex quis dui eleifend, dapibus vehicula lacus semper.", "Fusce ut libero consectetur, aliquet velit sed, hendrerit sem.", "Maecenas eget urna sagittis, efficitur arcu posuere, pellentesque metus", "Donec sit amet nisi ac tellus mattis venenatis volutpat sit amet mi"],
    "description": {
      "title": "Integer pharetra mi eu libero convallis ultricies",
      "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Donec auctor sapien eget sem blandit pharetra.", "In sed tellus congue, rhoncus mi quis, iaculis magna.", "Vestibulum at mauris semper, tristique nulla et, tristique nulla.", "Vestibulum at mauris semper, tristique nulla et, tristique nulla."]
    },
    "topics": [{
      "name": "Course Overview",
      "courseDetail": [{
        "demoVideoUrl": "https://www.youtube.com/watch?v=iY6qSRdMtZQ",
        "name": "Introduction",
        "time": "11.4"
      }, {
        "demoVideoUrl": "https://www.youtube.com/watch?v=A4coW3B8V2c",
        "name": "Applications",
        "time": "11.4"
      }, {
        "demoVideoUrl": "https://www.youtube.com/watch?v=1NpisqyBoI0",
        "name": "Installing",
        "time": "11.4"
      }]
    }, {
      "name": "Basic Programming",
      "courseDetail": [{
        "demoVideoUrl": "https://www.youtube.com/watch?v=yJMtLhvDfe4",
        "name": "Welcome to Section 2",
        "time": "3.24"
      }, {
        "demoVideoUrl": "https://www.youtube.com/watch?v=D7MudYw1Fhg",
        "name": "Basics",
        "time": "7.14"
      }, {
        "demoVideoUrl": "https://www.youtube.com/watch?v=0uAPzJVDVEo",
        "name": "Solo Practice",
        "time": "5.72"
      }]
    }, {
      "name": "Advance Topics",
      "courseDetail": [{
        "demoVideoUrl": "https://www.youtube.com/watch?v=Q2AOSQmV7w4",
        "name": "Advance Programming",
        "time": "10.66"
      }, {
        "demoVideoUrl": "https://www.youtube.com/watch?v=LESBbhL1TRw",
        "name": "Difference between Topics",
        "time": "11.4"
      }, {
        "demoVideoUrl": "https://www.youtube.com/watch?v=prXvKdPeDNU",
        "name": "Wrap Up",
        "time": "7.72"
      }]
    }],
    "instructorInformation": {
      "image": "/static/avatars/user-9.jpg",
      "name": "James Colt",
      "features": [{
        "icon": "video_library",
        "feature": "Lorem ipsum dolor sit amet."
      }, {
        "icon": "book",
        "feature": "Fusce ut libero consectetur."
      }, {
        "icon": "code",
        "feature": "Proin sit amet ex in quam."
      }, {
        "icon": "phone_iphone",
        "feature": "Donec sit amet nisi ac."
      }],
      "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    },
    "billingDetails": {
      "totalPrice": 1000,
      "discountPercent": 94,
      "discountTime": "2 Day",
      "guarntee": "30",
      "includes": [{
        "icon": "video_library",
        "feature": "Lorem ipsum dolor sit amet."
      }, {
        "icon": "book",
        "feature": "Fusce ut libero consectetur."
      }, {
        "icon": "code",
        "feature": "Proin sit amet ex in quam."
      }, {
        "icon": "phone_iphone",
        "feature": "Donec sit amet nisi ac."
      }]
    },
    "moreCourses": [{
      "image": "/static/img/course1.jpg",
      "bestseller": true,
      "videoDemoStatus": false,
      "demoVideoUrl": "",
      "name": "Lorem ipsum dolor sit amet sit amesdfffffft sit amet",
      "content": "Lorem ipsum",
      "oldPrice": "900",
      "rating": 5,
      "disount": 50
    }, {
      "image": "/static/img/course2.jpg",
      "bestseller": true,
      "videoDemoStatus": false,
      "demoVideoUrl": "",
      "name": "Lorem ipsum dosdaalor sit amet sit sfddddddamet sit amet",
      "content": "Lorem ipsum",
      "oldPrice": "900",
      "disount": 50,
      "rating": 5
    }, {
      "image": "/static/img/course3.jpg",
      "bestseller": true,
      "videoDemoStatus": true,
      "demoVideoUrl": "https://www.youtube.com/watch?v=kfgOX1Uh04M",
      "name": "Loadsrem ipsum dolor sit amet sit amet sit amet",
      "content": "Lorem ipsum",
      "oldPrice": "900",
      "disount": 50,
      "rating": 5
    }]
  }
});

/***/ }),

/***/ 1535:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(1536)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/courses/CourseWidgets/CourseBanner.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-601a10f8", Component.options)
  } else {
    hotAPI.reload("data-v-601a10f8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1536:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "banner-image-wrap courses-bg-img" }, [
    _c(
      "div",
      { staticClass: "banner-content-wrap fill-height bg-warn-overlay" },
      [
        _c(
          "v-layout",
          {
            attrs: {
              "align-center": "",
              "justify-center": "",
              row: "",
              "fill-height": ""
            }
          },
          [
            _c(
              "v-flex",
              { attrs: { xs9: "", sm9: "", md10: "", lg10: "", xl10: "" } },
              [
                _c("h2", { staticClass: "white--text" }, [
                  _vm._v("Learn With Your Convenience")
                ]),
                _vm._v(" "),
                _c("h4", { staticClass: "white--text" }, [
                  _vm._v(
                    "Learn any Course anywhere anytime from our 200 courses starting from $60 USD."
                  )
                ]),
                _vm._v(" "),
                _c(
                  "v-layout",
                  { attrs: { row: "", wrap: "", "ma-0": "" } },
                  [
                    _c(
                      "v-flex",
                      {
                        attrs: {
                          xs10: "",
                          sm6: "",
                          md3: "",
                          lg2: "",
                          xl3: "",
                          "pa-0": ""
                        }
                      },
                      [
                        _c(
                          "div",
                          { staticClass: "search" },
                          [
                            _c(
                              "v-form",
                              { staticClass: "search-form" },
                              [
                                _c("v-text-field", {
                                  attrs: {
                                    dark: "",
                                    color: "white",
                                    placeholder: "Find Your Course"
                                  }
                                })
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ]
                    )
                  ],
                  1
                )
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-601a10f8", module.exports)
  }
}

/***/ }),

/***/ 1552:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1553)
/* template */
var __vue_template__ = __webpack_require__(1554)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/courses/CourseWidgets/CourseCard.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-9bd39048", Component.options)
  } else {
    hotAPI.reload("data-v-9bd39048", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1553:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__data__ = __webpack_require__(1527);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__ = __webpack_require__(56);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['data', 'colxs', 'colsm', 'colmd', 'collg', 'colxl', 'width', 'height'],
  data: function data() {
    return {
      dialog: false,
      CourseData: __WEBPACK_IMPORTED_MODULE_0__data__["a" /* default */]
    };
  },
  methods: {
    getCurrentAppLayoutHandler: function getCurrentAppLayoutHandler() {
      return Object(__WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__["b" /* getCurrentAppLayout */])(this.$router);
    }
  }
});

/***/ }),

/***/ 1554:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("v-container", [
    _c(
      "div",
      { staticClass: "course-card layout row wrap" },
      _vm._l(_vm.data, function(list, key) {
        return _c(
          "app-card",
          {
            key: key,
            attrs: {
              customClasses: "course-item-wrap",
              colClasses:
                "xs" +
                _vm.colxs +
                " sm" +
                _vm.colsm +
                " md" +
                _vm.colmd +
                " lg" +
                _vm.collg +
                " xl" +
                _vm.colxl
            }
          },
          [
            _c(
              "div",
              { staticClass: "image-wrap" },
              [
                list.videoDemoStatus == false
                  ? [
                      _c(
                        "router-link",
                        {
                          attrs: {
                            to:
                              "/" +
                              (_vm.getCurrentAppLayoutHandler() +
                                "/courses/courses-detail")
                          }
                        },
                        [
                          _c("img", {
                            attrs: { src: list.image, alt: "image" }
                          })
                        ]
                      )
                    ]
                  : _vm._e(),
                _vm._v(" "),
                list.videoDemoStatus == true
                  ? [
                      _c(
                        "router-link",
                        {
                          attrs: {
                            to:
                              "/" +
                              (_vm.getCurrentAppLayoutHandler() +
                                "/courses/courses-detail")
                          }
                        },
                        [
                          _c("img", {
                            attrs: { src: list.image, alt: "image" }
                          })
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-dialog",
                        {
                          attrs: { width: "500" },
                          model: {
                            value: _vm.dialog,
                            callback: function($$v) {
                              _vm.dialog = $$v
                            },
                            expression: "dialog"
                          }
                        },
                        [
                          _c(
                            "v-btn",
                            {
                              attrs: { slot: "activator", icon: "" },
                              slot: "activator"
                            },
                            [_c("v-icon", [_vm._v("play_circle_filled")])],
                            1
                          ),
                          _vm._v(" "),
                          _c("iframe", {
                            attrs: {
                              src: list.demoVideoUrl,
                              frameborder: "0",
                              allowfullscreen: ""
                            }
                          })
                        ],
                        1
                      )
                    ]
                  : _vm._e(),
                _vm._v(" "),
                list.bestseller == true
                  ? [
                      _c(
                        "span",
                        {
                          staticClass:
                            "best-seller bestseller-tag d-inline-block"
                        },
                        [_vm._v(_vm._s(_vm.$t("message.bestseller")))]
                      )
                    ]
                  : _vm._e()
              ],
              2
            ),
            _vm._v(" "),
            _c(
              "router-link",
              {
                attrs: {
                  to:
                    "/" +
                    (_vm.getCurrentAppLayoutHandler() +
                      "/courses/courses-detail")
                }
              },
              [
                _c("h4", { staticClass: "make-ellipse" }, [
                  _vm._v(_vm._s(list.name))
                ])
              ]
            ),
            _vm._v(" "),
            _c("span", { staticClass: "fs-12 mb-3 d-block" }, [
              _vm._v(_vm._s(list.content))
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "rating-text layout row wrap ma-0" },
              [
                _c("v-rating", {
                  attrs: {
                    value: list.rating,
                    color: "warning",
                    "background-color": "warning"
                  }
                }),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(list.rating) + " Stars")])
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "price" }, [
              _c("h4", { attrs: { color: "primary" } }, [
                _vm._v(
                  "$" + _vm._s((list.oldPrice * (100 - list.disount)) / 100)
                )
              ]),
              _vm._v(" "),
              _c("del", [_vm._v("$" + _vm._s(list.oldPrice))])
            ]),
            _vm._v(" "),
            _c(
              "app-card",
              { attrs: { customClasses: "course-hover-item" } },
              [
                _c("div", { staticClass: "header" }, [
                  _c("div", { staticClass: "meta-info" }, [
                    _c("span", { staticClass: "date-info fs-12 fw-normal" }, [
                      _vm._v("Last updated: " + _vm._s(list.lastUpdates))
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "card-header" }, [
                    _c("h4", [
                      _c("a", { attrs: { href: "#" } }, [
                        _vm._v(_vm._s(list.name))
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "meta-info mb-1" }, [
                    list.bestseller == true
                      ? _c("div", { staticClass: "mb-1" }, [
                          _c(
                            "span",
                            { staticClass: "category fs-12 fw-normal" },
                            [
                              _c("span", { staticClass: "bestseller-tag" }, [
                                _vm._v("bestseller")
                              ]),
                              _vm._v(" " + _vm._s(list.bestSell))
                            ]
                          )
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("span", { staticClass: "meta-info-block" }, [
                      _c(
                        "span",
                        { staticClass: "lectures fs-12 fw-normal" },
                        [
                          _c("v-icon", { staticClass: "cmr-8" }, [
                            _vm._v("play_circle_filled")
                          ]),
                          _vm._v(_vm._s(list.lectures) + " lectures")
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "durations fs-12 fw-normal" },
                        [
                          _c("v-icon", { staticClass: "cmr-8" }, [
                            _vm._v("access_time")
                          ]),
                          _vm._v(_vm._s(list.hours) + " hours")
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "durations fs-12 fw-normal" },
                        [
                          _c("v-icon", { staticClass: "cmr-8" }, [
                            _vm._v("show_chart")
                          ]),
                          _vm._v(_vm._s(list.level) + " Levels")
                        ],
                        1
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "short-desc" }, [
                  _c("span", [_vm._v(_vm._s(list.describe))]),
                  _vm._v(" "),
                  _c(
                    "ul",
                    { staticClass: "course-list  my-3" },
                    _vm._l(list.features, function(feature, key) {
                      return _c("li", { key: key }, [_vm._v(_vm._s(feature))])
                    }),
                    0
                  )
                ]),
                _vm._v(" "),
                _c(
                  "v-btn",
                  {
                    staticClass: "error",
                    attrs: {
                      block: "",
                      to:
                        "/" +
                        (_vm.getCurrentAppLayoutHandler() + "/courses/sign-in")
                    }
                  },
                  [_vm._v(_vm._s(_vm.$t("message.addToCart")))]
                )
              ],
              1
            )
          ],
          1
        )
      }),
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-9bd39048", module.exports)
  }
}

/***/ }),

/***/ 1719:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__CourseWidgets_CourseBanner__ = __webpack_require__(1535);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__CourseWidgets_CourseBanner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__CourseWidgets_CourseBanner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__data__ = __webpack_require__(1527);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__CourseWidgets_CourseDetailLearn__ = __webpack_require__(1720);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__CourseWidgets_CourseDetailLearn___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__CourseWidgets_CourseDetailLearn__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__CourseWidgets_CourseDetailBanner__ = __webpack_require__(1723);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__CourseWidgets_CourseDetailBanner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__CourseWidgets_CourseDetailBanner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__CourseWidgets_CourseDetailDescription__ = __webpack_require__(1726);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__CourseWidgets_CourseDetailDescription___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__CourseWidgets_CourseDetailDescription__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__CourseWidgets_CourseDetailOverview__ = __webpack_require__(1729);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__CourseWidgets_CourseDetailOverview___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__CourseWidgets_CourseDetailOverview__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__CourseWidgets_CourseDetailInstructor__ = __webpack_require__(1732);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__CourseWidgets_CourseDetailInstructor___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__CourseWidgets_CourseDetailInstructor__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__CourseWidgets_CourseCard__ = __webpack_require__(1552);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__CourseWidgets_CourseCard___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__CourseWidgets_CourseCard__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__CourseWidgets_CourseDetailBilling__ = __webpack_require__(1735);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__CourseWidgets_CourseDetailBilling___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__CourseWidgets_CourseDetailBilling__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//









/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CourseData: __WEBPACK_IMPORTED_MODULE_1__data__["a" /* default */]
    };
  },
  components: {
    CourseBanner: __WEBPACK_IMPORTED_MODULE_0__CourseWidgets_CourseBanner___default.a,
    CourseDetailBanner: __WEBPACK_IMPORTED_MODULE_3__CourseWidgets_CourseDetailBanner___default.a,
    CourseDetailLearn: __WEBPACK_IMPORTED_MODULE_2__CourseWidgets_CourseDetailLearn___default.a,
    CourseDetailDesciption: __WEBPACK_IMPORTED_MODULE_4__CourseWidgets_CourseDetailDescription___default.a,
    CourseDetailOverview: __WEBPACK_IMPORTED_MODULE_5__CourseWidgets_CourseDetailOverview___default.a,
    CourseDetailInstructor: __WEBPACK_IMPORTED_MODULE_6__CourseWidgets_CourseDetailInstructor___default.a,
    CourseCard: __WEBPACK_IMPORTED_MODULE_7__CourseWidgets_CourseCard___default.a,
    CourseDetailBilling: __WEBPACK_IMPORTED_MODULE_8__CourseWidgets_CourseDetailBilling___default.a
  }
});

/***/ }),

/***/ 1720:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1721)
/* template */
var __vue_template__ = __webpack_require__(1722)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-115edcd7", Component.options)
  } else {
    hotAPI.reload("data-v-115edcd7", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1721:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__data__ = __webpack_require__(1527);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CourseData: __WEBPACK_IMPORTED_MODULE_0__data__["a" /* default */]
    };
  }
});

/***/ }),

/***/ 1722:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "app-card",
    {
      staticClass: "course-info-wrap",
      attrs: {
        heading: _vm.$t("message.whatYoWillLearn"),
        colClasses: "xs12 sm12 md12 lg12 xl12",
        contentCustomClass: "pt-0"
      }
    },
    [
      _c("div", { staticClass: "course-info-box" }, [
        _c(
          "ul",
          { staticClass: "info-box-list" },
          _vm._l(_vm.CourseData.courseDetail.learn, function(info, key) {
            return _c("li", { key: key }, [
              _c("i", { staticClass: "material-icons" }, [_vm._v("check")]),
              _c("span", [_vm._v(_vm._s(info))])
            ])
          }),
          0
        )
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-115edcd7", module.exports)
  }
}

/***/ }),

/***/ 1723:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1724)
/* template */
var __vue_template__ = __webpack_require__(1725)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b4b645ae", Component.options)
  } else {
    hotAPI.reload("data-v-b4b645ae", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1724:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__data__ = __webpack_require__(1527);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CourseData: __WEBPACK_IMPORTED_MODULE_0__data__["a" /* default */],
      dialog: false
    };
  }
});

/***/ }),

/***/ 1725:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "banner-image-wrap banner-detail grey darken-4 ma-0" },
    [
      _c("v-container", { attrs: { "grid-list-xl": "", fluid: "" } }, [
        _c(
          "div",
          { staticClass: "banner-content-wrap" },
          [
            _c(
              "v-layout",
              {
                attrs: {
                  row: "",
                  wrap: "",
                  "align-center": "",
                  "justify-center": "",
                  "fill-height": ""
                }
              },
              [
                _c(
                  "v-flex",
                  {
                    attrs: { xs12: "", sm12: "", md12: "", lg12: "", xl12: "" }
                  },
                  [
                    _c(
                      "v-layout",
                      {
                        attrs: {
                          row: "",
                          wrap: "",
                          "align-center": "",
                          "justify-center": "",
                          "filll-height": ""
                        }
                      },
                      [
                        _c(
                          "v-flex",
                          {
                            attrs: {
                              xs12: "",
                              sm12: "",
                              md7: "",
                              lg7: "",
                              xl8: ""
                            }
                          },
                          [
                            _c("h2", { staticClass: "white--text" }, [
                              _vm._v(
                                _vm._s(_vm.CourseData.courseDetail.heading)
                              )
                            ]),
                            _vm._v(" "),
                            _c("h4", { staticClass: "white--text" }, [
                              _vm._v(
                                _vm._s(_vm.CourseData.courseDetail.content)
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "meta-info" }, [
                              _c("p", { staticClass: "white--text" }, [
                                _c(
                                  "span",
                                  {
                                    staticClass:
                                      "bestseller-tag d-inline-block mr-3 mb-1"
                                  },
                                  [_vm._v(_vm._s(_vm.$t("message.bestseller")))]
                                ),
                                _vm._v(" "),
                                _c("span", {
                                  staticClass: "rating-wrap d-inline-block"
                                }),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    staticClass: "student-count d-inline-block"
                                  },
                                  [
                                    _c(
                                      "span",
                                      { staticClass: "count font-weight-bold" },
                                      [
                                        _vm._v(
                                          _vm._s(
                                            _vm.CourseData.courseDetail
                                              .bestSeller
                                          )
                                        )
                                      ]
                                    ),
                                    _vm._v(" students enrolled")
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("p", { staticClass: "white--text" }, [
                                _c("span", { staticClass: "white--text" }, [
                                  _vm._v("Created By ")
                                ]),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    staticClass:
                                      "instructor-name d-inline-block"
                                  },
                                  [
                                    _c(
                                      "a",
                                      {
                                        staticClass: "white--text",
                                        attrs: { href: "#" }
                                      },
                                      [
                                        _vm._v(
                                          " " +
                                            _vm._s(
                                              _vm.CourseData.courseDetail
                                                .createdBy
                                            ) +
                                            " "
                                        )
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    staticClass: "updated-time d-inline-block"
                                  },
                                  [
                                    _vm._v(
                                      " Last Updated " +
                                        _vm._s(
                                          _vm.CourseData.courseDetail
                                            .lastUpdates
                                        )
                                    )
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("p", { staticClass: "white--text" }, [
                                _c(
                                  "span",
                                  {
                                    staticClass:
                                      "language layout align-start ma-0"
                                  },
                                  [
                                    _c("v-icon", { staticClass: "cmr-8" }, [
                                      _vm._v("chat_bubble_outline")
                                    ]),
                                    _vm._v(
                                      _vm._s(
                                        _vm.CourseData.courseDetail.language
                                      )
                                    )
                                  ],
                                  1
                                )
                              ])
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "v-flex",
                          {
                            attrs: {
                              xs12: "",
                              sm6: "",
                              md5: "",
                              lg4: "",
                              xl3: ""
                            }
                          },
                          [
                            _c(
                              "app-card",
                              { attrs: { contentCustomClass: "pa-3" } },
                              [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "video-wrap overlay-wrap d-inline-flex"
                                  },
                                  [
                                    _c("img", {
                                      staticClass: "banner-video",
                                      attrs: {
                                        src: "/static/img/about2.png",
                                        width: "230",
                                        height: "235",
                                        alt: "video"
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "overlay-content layout align-center justify-center"
                                      },
                                      [
                                        _c(
                                          "v-dialog",
                                          {
                                            attrs: {
                                              width: "500",
                                              height: "300"
                                            },
                                            model: {
                                              value: _vm.dialog,
                                              callback: function($$v) {
                                                _vm.dialog = $$v
                                              },
                                              expression: "dialog"
                                            }
                                          },
                                          [
                                            _c(
                                              "v-btn",
                                              {
                                                attrs: {
                                                  slot: "activator",
                                                  icon: ""
                                                },
                                                slot: "activator"
                                              },
                                              [
                                                _c("v-icon", [
                                                  _vm._v("play_circle_filled")
                                                ])
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c("iframe", {
                                              attrs: {
                                                src:
                                                  _vm.CourseData.courseDetail
                                                    .demoVideoUrl,
                                                frameborder: "0",
                                                allowfullscreen: ""
                                              }
                                            })
                                          ],
                                          1
                                        )
                                      ],
                                      1
                                    )
                                  ]
                                )
                              ]
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-b4b645ae", module.exports)
  }
}

/***/ }),

/***/ 1726:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1727)
/* template */
var __vue_template__ = __webpack_require__(1728)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3443ce4f", Component.options)
  } else {
    hotAPI.reload("data-v-3443ce4f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1727:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__data__ = __webpack_require__(1527);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CourseData: __WEBPACK_IMPORTED_MODULE_0__data__["a" /* default */]
    };
  }
});

/***/ }),

/***/ 1728:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "app-card",
    {
      staticClass: "detail-dec-wrap",
      attrs: {
        colClasses: "xs12 sm12 md12 lg12 xl12",
        heading: _vm.$t("message.description"),
        contentCustomClass: "pt-0"
      }
    },
    [
      _c("p", { staticClass: "font-weight-bold mrgn-b-sm" }, [
        _vm._v(_vm._s(_vm.CourseData.courseDetail.description.title))
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(_vm._s(_vm.CourseData.courseDetail.description.content))
      ]),
      _vm._v(" "),
      _c(
        "ul",
        _vm._l(_vm.CourseData.courseDetail.description.features, function(
          feature,
          key
        ) {
          return _c("li", { key: key }, [_vm._v(_vm._s(feature))])
        }),
        0
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3443ce4f", module.exports)
  }
}

/***/ }),

/***/ 1729:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1730)
/* template */
var __vue_template__ = __webpack_require__(1731)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e668d094", Component.options)
  } else {
    hotAPI.reload("data-v-e668d094", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1730:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__data__ = __webpack_require__(1527);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__ = __webpack_require__(56);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CourseData: __WEBPACK_IMPORTED_MODULE_0__data__["a" /* default */]
    };
  },
  methods: {
    getCurrentAppLayoutHandler: function getCurrentAppLayoutHandler() {
      return Object(__WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__["b" /* getCurrentAppLayout */])(this.$router);
    }
  }
});

/***/ }),

/***/ 1731:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "flex xs12 sm12 md12 lg12 xl12 courses-overview" },
    _vm._l(_vm.CourseData.courseDetail.topics, function(list, key) {
      return _c(
        "v-expansion-panel",
        { key: key },
        [
          _c(
            "v-expansion-panel-content",
            [
              _c(
                "div",
                {
                  staticClass: "layout mt-0 justify-start",
                  attrs: { slot: "header" },
                  slot: "header"
                },
                [_vm._v("\n\t\t\t\t\t" + _vm._s(list.name) + "\n\t\t\t\t")]
              ),
              _vm._v(" "),
              _vm._l(list.courseDetail, function(details, key) {
                return _c(
                  "v-list",
                  { key: key },
                  [
                    _c(
                      "v-list-tile",
                      {
                        attrs: {
                          to:
                            "/" +
                            (_vm.getCurrentAppLayoutHandler() +
                              "/courses/courses-detail")
                        }
                      },
                      [
                        _c(
                          "v-list-tile-action",
                          [_c("v-icon", [_vm._v("play_circle_filled")])],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "v-list-tile-content",
                          [
                            _c(
                              "v-list-tile-title",
                              {
                                staticClass:
                                  "ma-0 layout row wrap justify-space-between"
                              },
                              [
                                _c(
                                  "v-flex",
                                  {
                                    attrs: {
                                      xs12: "",
                                      sm12: "",
                                      md12: "",
                                      lg12: "",
                                      xl8: "",
                                      "py-0": "",
                                      "pl-0": ""
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "\n\t\t\t\t\t\t\t\t" +
                                        _vm._s(details.name) +
                                        "\n\t\t\t\t\t\t\t\t"
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "v-flex",
                                  {
                                    attrs: {
                                      xs12: "",
                                      sm12: "",
                                      md12: "",
                                      lg12: "",
                                      xl4: "",
                                      "py-0": ""
                                    }
                                  },
                                  [
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "ma-0 layout row wrap justify-space-between"
                                      },
                                      [
                                        _c("div", [
                                          _c(
                                            "a",
                                            {
                                              staticClass: "fs-12 fw-normal",
                                              attrs: { href: "#" }
                                            },
                                            [_vm._v("Preview")]
                                          )
                                        ]),
                                        _vm._v(" "),
                                        _c("div", [
                                          _c(
                                            "span",
                                            { staticClass: "fs-12 fw-normal" },
                                            [_vm._v(_vm._s(details.time))]
                                          )
                                        ])
                                      ]
                                    )
                                  ]
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("v-divider")
                  ],
                  1
                )
              })
            ],
            2
          )
        ],
        1
      )
    }),
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-e668d094", module.exports)
  }
}

/***/ }),

/***/ 1732:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1733)
/* template */
var __vue_template__ = __webpack_require__(1734)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-feb1d8cc", Component.options)
  } else {
    hotAPI.reload("data-v-feb1d8cc", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1733:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__data__ = __webpack_require__(1527);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CourseData: __WEBPACK_IMPORTED_MODULE_0__data__["a" /* default */]
    };
  }
});

/***/ }),

/***/ 1734:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.CourseData.courseDetail
    ? _c(
        "app-card",
        {
          staticClass: "about-instructor",
          attrs: {
            heading: _vm.$t("message.aboutInstructor"),
            colClasses: "xs12 sm12 md12 lg12 xl12",
            contentCustomClass: "pt-0"
          }
        },
        [
          _c(
            "div",
            {
              staticClass:
                "layout row wrap align-center justify-start mx-0 mt-0 mb-3"
            },
            [
              _c("div", { staticClass: "image-wrap mb-2" }, [
                _c("img", {
                  attrs: {
                    src: _vm.CourseData.courseDetail.instructorInformation.image
                  }
                })
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "instructor-meta" },
                [
                  _c("h4", [
                    _vm._v(
                      _vm._s(
                        _vm.CourseData.courseDetail.instructorInformation.name
                      )
                    )
                  ]),
                  _vm._v(" "),
                  _vm._l(
                    _vm.CourseData.courseDetail.instructorInformation.features,
                    function(feature, key) {
                      return _c(
                        "v-list",
                        { key: key, staticClass: "incentive-list" },
                        [
                          _c(
                            "v-list-tile",
                            [
                              _c(
                                "v-list-tile-action",
                                [_c("v-icon", [_vm._v(_vm._s(feature.icon))])],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-list-tile-content",
                                [
                                  _c("v-list-tile-title", [
                                    _c("p", { staticClass: "mb-0" }, [
                                      _vm._v(_vm._s(feature.feature))
                                    ])
                                  ])
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    }
                  )
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c("p", [
            _vm._v(
              _vm._s(_vm.CourseData.courseDetail.instructorInformation.content)
            )
          ])
        ]
      )
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-feb1d8cc", module.exports)
  }
}

/***/ }),

/***/ 1735:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1736)
/* template */
var __vue_template__ = __webpack_require__(1737)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-ee81aa64", Component.options)
  } else {
    hotAPI.reload("data-v-ee81aa64", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1736:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__data__ = __webpack_require__(1527);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__ = __webpack_require__(56);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CourseData: __WEBPACK_IMPORTED_MODULE_0__data__["a" /* default */]
    };
  },
  methods: {
    getCurrentAppLayoutHandler: function getCurrentAppLayoutHandler() {
      return Object(__WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__["b" /* getCurrentAppLayout */])(this.$router);
    }
  }
});

/***/ }),

/***/ 1737:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "app-card",
    { staticClass: "course-detail-billing-wrap cpb-24" },
    [
      _c("div", { staticClass: "price-wrap" }, [
        _c("span", { staticClass: "cmr-8 discount-price d-inline-block" }, [
          _vm._v(
            "$" +
              _vm._s(
                (_vm.CourseData.courseDetail.billingDetails.totalPrice *
                  (100 -
                    _vm.CourseData.courseDetail.billingDetails
                      .discountPercent)) /
                  100
              )
          )
        ]),
        _vm._v(" "),
        _c("span", { staticClass: "cmr-8 base-price d-inline-block" }, [
          _c("del", [
            _vm._v(
              "$" +
                _vm._s(_vm.CourseData.courseDetail.billingDetails.totalPrice)
            )
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticClass: "discount-value d-inline-block" }, [
          _vm._v(
            _vm._s(_vm.CourseData.courseDetail.billingDetails.discountPercent) +
              " % off"
          )
        ])
      ]),
      _vm._v(" "),
      _c(
        "p",
        { staticClass: "price-duration error--text " },
        [
          _c("v-icon", { staticClass: "cmr-8" }, [_vm._v("timer")]),
          _vm._v(" "),
          _c("span", { staticClass: "font-weight-bold cmr-8" }, [
            _vm._v(
              " " +
                _vm._s(
                  _vm.CourseData.courseDetail.billingDetails.discountTime
                ) +
                " "
            )
          ]),
          _vm._v(" left at this price!\n   ")
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "button-wrap" },
        [
          _c(
            "v-btn",
            {
              staticClass: "error pa-4",
              attrs: {
                to:
                  "/" + (_vm.getCurrentAppLayoutHandler() + "/courses/sign-in"),
                block: ""
              }
            },
            [_vm._v(_vm._s(_vm.$t("message.addToCart")) + "\n      ")]
          ),
          _vm._v(" "),
          _c("p", { staticClass: "text-center" }, [
            _vm._v(
              _vm._s(_vm.CourseData.courseDetail.billingDetails.guarntee) +
                "day money back garuntee"
            )
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "incentives" },
        [
          _c("h4", [_vm._v("Includes")]),
          _vm._v(" "),
          _vm._l(_vm.CourseData.courseDetail.billingDetails.includes, function(
            feature,
            key
          ) {
            return _c(
              "v-list",
              { key: key, staticClass: "incentive-list" },
              [
                _c(
                  "v-list-tile",
                  [
                    _c(
                      "v-list-tile-action",
                      [_c("v-icon", [_vm._v(_vm._s(feature.icon))])],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "v-list-tile-content",
                      [
                        _c("v-list-tile-title", [
                          _c("p", { staticClass: "mb-0" }, [
                            _vm._v(_vm._s(feature.feature))
                          ])
                        ])
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          })
        ],
        2
      ),
      _vm._v(" "),
      _c("div", { staticClass: "coupon-available text-center" }, [
        _c(
          "a",
          { staticClass: "accent-text", attrs: { href: "javascript:void(0)" } },
          [_vm._v("Have a coupon?")]
        )
      ]),
      _vm._v(" "),
      _c("v-divider"),
      _vm._v(" "),
      _c("div", { staticClass: "coupon-available text-center px-3" }, [
        _c(
          "a",
          { staticClass: "accent-text", attrs: { href: "javascript:void(0)" } },
          [_vm._v("Share")]
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-ee81aa64", module.exports)
  }
}

/***/ }),

/***/ 1738:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "courses-detail-wrap" },
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { "grid-list-xl": "", fluid: "" } },
        [
          _c("course-detail-banner"),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "course-detail" },
            [
              _c(
                "v-layout",
                {
                  attrs: {
                    row: "",
                    wrap: "",
                    "justify-center": "",
                    "align-center": ""
                  }
                },
                [
                  _c(
                    "v-flex",
                    {
                      attrs: {
                        xs12: "",
                        sm12: "",
                        md12: "",
                        lg12: "",
                        xl12: ""
                      }
                    },
                    [
                      _c(
                        "v-layout",
                        { attrs: { row: "", wrap: "" } },
                        [
                          _c(
                            "v-flex",
                            {
                              attrs: {
                                xs12: "",
                                sm12: "",
                                md12: "",
                                lg9: "",
                                xl9: ""
                              }
                            },
                            [
                              _c(
                                "v-layout",
                                { attrs: { row: "", wrap: "" } },
                                [
                                  _c("course-detail-learn"),
                                  _vm._v(" "),
                                  _c("course-detail-desciption"),
                                  _vm._v(" "),
                                  _c("course-detail-overview"),
                                  _vm._v(" "),
                                  _c("course-detail-instructor"),
                                  _vm._v(" "),
                                  _c(
                                    "app-card",
                                    {
                                      attrs: {
                                        colClasses: "xs12 sm12 md12 lg12 xl12",
                                        customClasses: "more-courses-grid",
                                        contentCustomClass:
                                          "layout row wrap pt-0",
                                        heading: _vm.$t(
                                          "message.moreCoursesFromJamesColt"
                                        )
                                      }
                                    },
                                    [
                                      _c(
                                        "v-layout",
                                        {
                                          attrs: {
                                            row: "",
                                            wrap: "",
                                            "align-start": "",
                                            "justify-start": "",
                                            "mx-0": "",
                                            "mb-0": ""
                                          }
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "course-item-wrap" },
                                            [
                                              _c("course-card", {
                                                attrs: {
                                                  height: 200,
                                                  width: 335,
                                                  data:
                                                    _vm.CourseData.courseDetail
                                                      .moreCourses,
                                                  cols: 6,
                                                  colxl: 4,
                                                  collg: 4,
                                                  colmd: 4,
                                                  colsm: 6,
                                                  colxs: 12
                                                }
                                              })
                                            ],
                                            1
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-flex",
                            {
                              attrs: {
                                xs12: "",
                                sm12: "",
                                md12: "",
                                lg3: "",
                                xl3: "",
                                "course-sidebar": ""
                              }
                            },
                            [
                              _c(
                                "div",
                                { staticClass: "custom-height-auto" },
                                [
                                  _c("course-detail-billing"),
                                  _vm._v(" "),
                                  _c(
                                    "app-card",
                                    {
                                      attrs: {
                                        contentCustomClass: "pt-0",
                                        heading: "Contrary to popular belief ?"
                                      }
                                    },
                                    [
                                      _c("p", [
                                        _vm._v(
                                          "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "a",
                                        {
                                          staticClass:
                                            "font-weight-bold primary-text",
                                          attrs: { href: "#" }
                                        },
                                        [_vm._v("Nulla eu augue !")]
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-42209f83", module.exports)
  }
}

/***/ })

});