webpackJsonp([167],{

/***/ 1475:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(2063)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/timelines/SmallDots.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3ce40125", Component.options)
  } else {
    hotAPI.reload("data-v-3ce40125", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 2063:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "hover-wrapper" },
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { "grid-list-xl": "", "pt-0": "" } },
        [
          _c(
            "v-layout",
            { attrs: { row: "", wrap: "" } },
            [
              _c(
                "app-card",
                { attrs: { colClasses: "xl12 lg12 md12 sm12 xs12" } },
                [
                  _c("div", { staticClass: "mb-4" }, [
                    _c("p", [
                      _vm._v(
                        "Easily alternate styles to provide a unique design."
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-timeline",
                    [
                      _c(
                        "v-timeline-item",
                        {
                          attrs: {
                            color: "purple lighten-2",
                            "fill-dot": "",
                            right: ""
                          }
                        },
                        [
                          _c(
                            "v-card",
                            [
                              _c(
                                "v-card-title",
                                { staticClass: "purple lighten-2" },
                                [
                                  _c(
                                    "v-icon",
                                    {
                                      staticClass: "mr-3 white--text",
                                      attrs: { size: "42" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                              mdi-magnify\n                           "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "h2",
                                    {
                                      staticClass:
                                        "display-1 white--text font-weight-light"
                                    },
                                    [_vm._v("Title 1")]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-container",
                                [
                                  _c(
                                    "v-layout",
                                    [
                                      _c("v-flex", { attrs: { xs10: "" } }, [
                                        _vm._v(
                                          "\n                                 Lorem ipsum dolor sit amet, no nam oblique veritus. Commune scaevola imperdiet nec ut, sed euismod convenire principes at. Est et nobis iisque percipit.\n                              "
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "v-flex",
                                        { attrs: { xs2: "" } },
                                        [
                                          _c(
                                            "v-icon",
                                            { attrs: { size: "64" } },
                                            [_vm._v("mdi-calendar-text")]
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-timeline-item",
                        {
                          attrs: {
                            color: "amber lighten-1",
                            "fill-dot": "",
                            left: "",
                            small: ""
                          }
                        },
                        [
                          _c(
                            "v-card",
                            [
                              _c(
                                "v-card-title",
                                { staticClass: "amber lighten-1 justify-end" },
                                [
                                  _c(
                                    "h2",
                                    {
                                      staticClass:
                                        "display-1 mr-3 white--text font-weight-light"
                                    },
                                    [_vm._v("Title 2")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-icon",
                                    {
                                      staticClass: "white--text",
                                      attrs: { size: "42" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                              mdi-home-outline\n                           "
                                      )
                                    ]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-container",
                                [
                                  _c(
                                    "v-layout",
                                    [
                                      _c("v-flex", { attrs: { xs8: "" } }, [
                                        _vm._v(
                                          "\n                                 Lorem ipsum dolor sit amet, no nam oblique veritus. Commune scaevola imperdiet nec ut, sed euismod convenire principes at. Est et nobis iisque percipit.\n                              "
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("v-flex", { attrs: { xs4: "" } }, [
                                        _vm._v(
                                          "\n                                 Lorem ipsum dolor sit amet, no nam oblique veritus.\n                              "
                                        )
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-timeline-item",
                        {
                          attrs: {
                            color: "cyan lighten-1",
                            "fill-dot": "",
                            right: ""
                          }
                        },
                        [
                          _c(
                            "v-card",
                            [
                              _c(
                                "v-card-title",
                                { staticClass: "cyan lighten-1" },
                                [
                                  _c(
                                    "v-icon",
                                    {
                                      staticClass: "mr-3 white--text",
                                      attrs: { size: "42" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                              mdi-email-outline\n                           "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "h2",
                                    {
                                      staticClass:
                                        "display-1 white--text font-weight-light"
                                    },
                                    [_vm._v("Title 3")]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-container",
                                [
                                  _c(
                                    "v-layout",
                                    _vm._l(3, function(n) {
                                      return _c(
                                        "v-flex",
                                        { key: n, attrs: { xs4: "" } },
                                        [
                                          _vm._v(
                                            "\n                                 Lorem ipsum dolor sit amet, no nam oblique veritus no nam oblique.\n                              "
                                          )
                                        ]
                                      )
                                    }),
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-timeline-item",
                        {
                          attrs: {
                            color: "red lighten-1",
                            "fill-dot": "",
                            left: "",
                            small: ""
                          }
                        },
                        [
                          _c(
                            "v-card",
                            [
                              _c(
                                "v-card-title",
                                { staticClass: "red lighten-1 justify-end" },
                                [
                                  _c(
                                    "h2",
                                    {
                                      staticClass:
                                        "display-1 mr-3 white--text font-weight-light"
                                    },
                                    [_vm._v("Title 4")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-icon",
                                    {
                                      staticClass: "white--text",
                                      attrs: { size: "42" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                              mdi-account-multiple-outline\n                           "
                                      )
                                    ]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-container",
                                [
                                  _c(
                                    "v-layout",
                                    [
                                      _c(
                                        "v-flex",
                                        { attrs: { xs2: "" } },
                                        [
                                          _c(
                                            "v-icon",
                                            { attrs: { size: "64" } },
                                            [_vm._v("mdi-server-network")]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c("v-flex", { attrs: { xs10: "" } }, [
                                        _vm._v(
                                          "\n                                 Lorem ipsum dolor sit amet, no nam oblique veritus. Commune scaevola imperdiet nec ut, sed euismod convenire principes at. Est et nobis iisque percipit, an vim zril disputando voluptatibus.\n                              "
                                        )
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-timeline-item",
                        {
                          attrs: {
                            color: "green lighten-1",
                            "fill-dot": "",
                            right: ""
                          }
                        },
                        [
                          _c(
                            "v-card",
                            [
                              _c(
                                "v-card-title",
                                { staticClass: "green lighten-1" },
                                [
                                  _c(
                                    "v-icon",
                                    {
                                      staticClass: "mr-3 white--text",
                                      attrs: { size: "42" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                              mdi-phone-in-talk\n                           "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "h2",
                                    {
                                      staticClass:
                                        "display-1 white--text font-weight-light"
                                    },
                                    [_vm._v("Title 5")]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-container",
                                [
                                  _c(
                                    "v-layout",
                                    [
                                      _c("v-flex", [
                                        _vm._v(
                                          "\n                                 Lorem ipsum dolor sit amet, no nam oblique veritus. Commune scaevola imperdiet nec ut, sed euismod convenire principes at. Est et nobis iisque percipit, an vim zril disputando voluptatibus, vix an salutandi sententiae.\n                              "
                                        )
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3ce40125", module.exports)
  }
}

/***/ })

});