webpackJsonp([35],{

/***/ 1416:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1739)
/* template */
var __vue_template__ = __webpack_require__(1740)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/courses/SignIn.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5a7b0858", Component.options)
  } else {
    hotAPI.reload("data-v-5a7b0858", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1535:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(1536)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/courses/CourseWidgets/CourseBanner.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-601a10f8", Component.options)
  } else {
    hotAPI.reload("data-v-601a10f8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1536:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "banner-image-wrap courses-bg-img" }, [
    _c(
      "div",
      { staticClass: "banner-content-wrap fill-height bg-warn-overlay" },
      [
        _c(
          "v-layout",
          {
            attrs: {
              "align-center": "",
              "justify-center": "",
              row: "",
              "fill-height": ""
            }
          },
          [
            _c(
              "v-flex",
              { attrs: { xs9: "", sm9: "", md10: "", lg10: "", xl10: "" } },
              [
                _c("h2", { staticClass: "white--text" }, [
                  _vm._v("Learn With Your Convenience")
                ]),
                _vm._v(" "),
                _c("h4", { staticClass: "white--text" }, [
                  _vm._v(
                    "Learn any Course anywhere anytime from our 200 courses starting from $60 USD."
                  )
                ]),
                _vm._v(" "),
                _c(
                  "v-layout",
                  { attrs: { row: "", wrap: "", "ma-0": "" } },
                  [
                    _c(
                      "v-flex",
                      {
                        attrs: {
                          xs10: "",
                          sm6: "",
                          md3: "",
                          lg2: "",
                          xl3: "",
                          "pa-0": ""
                        }
                      },
                      [
                        _c(
                          "div",
                          { staticClass: "search" },
                          [
                            _c(
                              "v-form",
                              { staticClass: "search-form" },
                              [
                                _c("v-text-field", {
                                  attrs: {
                                    dark: "",
                                    color: "white",
                                    placeholder: "Find Your Course"
                                  }
                                })
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ]
                    )
                  ],
                  1
                )
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-601a10f8", module.exports)
  }
}

/***/ }),

/***/ 1739:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__CourseWidgets_CourseBanner__ = __webpack_require__(1535);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__CourseWidgets_CourseBanner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__CourseWidgets_CourseBanner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__ = __webpack_require__(56);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    CourseBanner: __WEBPACK_IMPORTED_MODULE_0__CourseWidgets_CourseBanner___default.a
  },
  methods: {
    getCurrentAppLayoutHandler: function getCurrentAppLayoutHandler() {
      return Object(__WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__["b" /* getCurrentAppLayout */])(this.$router);
    }
  }
});

/***/ }),

/***/ 1740:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "courses-signin" },
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "" } },
        [
          _c("course-banner"),
          _vm._v(" "),
          _c(
            "v-layout",
            {
              attrs: {
                row: "",
                wrap: "",
                "align-center": "",
                "justify-center": "",
                "signIn-wrap": ""
              }
            },
            [
              _c(
                "v-flex",
                { attrs: { xs12: "", sm12: "", md10: "", lg10: "", xl9: "" } },
                [
                  _c(
                    "div",
                    { staticClass: "shop-by-category section-gap pad-y-lg" },
                    [
                      _c(
                        "v-layout",
                        { attrs: { row: "", wrap: "", "justify-center": "" } },
                        [
                          _c(
                            "v-flex",
                            {
                              attrs: {
                                xs11: "",
                                sm8: "",
                                md6: "",
                                lg6: "",
                                xl6: ""
                              }
                            },
                            [
                              _c(
                                "app-card",
                                {
                                  attrs: {
                                    contentCustomClass: "pt-0",
                                    heading: _vm.$t("message.userSignIn")
                                  }
                                },
                                [
                                  _c(
                                    "v-form",
                                    [
                                      _c(
                                        "div",
                                        { staticClass: "mrgn-b-md" },
                                        [
                                          _c("v-text-field", {
                                            attrs: {
                                              placeholder: "Email*",
                                              type: "email",
                                              required: ""
                                            }
                                          })
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        { staticClass: "mrgn-b-md" },
                                        [
                                          _c("v-text-field", {
                                            attrs: {
                                              placeholder: "Password*",
                                              type: "password",
                                              required: ""
                                            }
                                          })
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "v-layout",
                                        {
                                          attrs: {
                                            row: "",
                                            wrap: "",
                                            "align-start": "",
                                            "justify-center": "",
                                            "mx-0": ""
                                          }
                                        },
                                        [
                                          _c("v-checkbox", {
                                            attrs: { label: "Remember Me" }
                                          }),
                                          _vm._v(" "),
                                          _c(
                                            "router-link",
                                            {
                                              staticClass: "mt-3",
                                              attrs: {
                                                to: "/session/forgot-password"
                                              }
                                            },
                                            [
                                              _vm._v(
                                                "\n                                 Forgot Password ?\n                              "
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "v-btn",
                                        {
                                          staticClass: "danger mx-0 mb-4",
                                          attrs: {
                                            color: "error",
                                            to: "/session/login"
                                          }
                                        },
                                        [
                                          _vm._v(
                                            _vm._s(_vm.$t("message.signIn"))
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "p",
                                        [
                                          _vm._v("Don't have an account? "),
                                          _c(
                                            "router-link",
                                            {
                                              staticClass: "primary-text",
                                              attrs: { to: "/session/sign-up" }
                                            },
                                            [_vm._v("Click here to create one")]
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-flex",
                            {
                              attrs: {
                                xs11: "",
                                sm8: "",
                                md6: "",
                                lg6: "",
                                xl6: ""
                              }
                            },
                            [
                              _c(
                                "app-card",
                                {
                                  attrs: {
                                    heading: _vm.$t("message.guestCheckout"),
                                    contentCustomClass: "pt-0"
                                  }
                                },
                                [
                                  _c("p", [
                                    _vm._v(
                                      "Proceed to checkout and create an account later."
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "v-btn",
                                    {
                                      staticClass: "error mx-0",
                                      attrs: {
                                        to:
                                          "/" +
                                          (_vm.getCurrentAppLayoutHandler() +
                                            "/courses/payment")
                                      }
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm.$t("message.continueAsGuest")
                                        )
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5a7b0858", module.exports)
  }
}

/***/ })

});