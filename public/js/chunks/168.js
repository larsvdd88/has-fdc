webpackJsonp([168],{

/***/ 1477:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(2066)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/timelines/ColoredDots.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5443cd40", Component.options)
  } else {
    hotAPI.reload("data-v-5443cd40", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 2066:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "hover-wrapper colored-dots-wrapper" },
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { "custom-flex": "", "grid-list-xl": "", "pt-0": "" } },
        [
          _c(
            "v-layout",
            { attrs: { row: "", wrap: "" } },
            [
              _c(
                "app-card",
                { attrs: { colClasses: "xl12 lg12 md12 sm12 xs12" } },
                [
                  _c("div", { staticClass: "mb-4" }, [
                    _c("p", [
                      _vm._v(
                        "Colors dots create visual breakpoints that make your timelines easier to read."
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-card",
                    { staticClass: "mx-auto", attrs: { "max-width": "400" } },
                    [
                      _c(
                        "v-card",
                        { staticClass: "white--text", attrs: { flat: "" } },
                        [
                          _c(
                            "v-btn",
                            {
                              attrs: {
                                absolute: "",
                                bottom: "",
                                color: "pink",
                                right: "",
                                fab: ""
                              }
                            },
                            [
                              _c("v-icon", { staticClass: "white--text" }, [
                                _vm._v("mdi-plus")
                              ])
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-card-title",
                            { staticClass: "pa-2 purple lighten-3" },
                            [
                              _c(
                                "v-btn",
                                { attrs: { icon: "" } },
                                [
                                  _c("v-icon", { staticClass: "white--text" }, [
                                    _vm._v("mdi-menu")
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "h3",
                                {
                                  staticClass:
                                    "title font-weight-light text-xs-center grow"
                                },
                                [_vm._v("Timeline")]
                              ),
                              _vm._v(" "),
                              _c(
                                "v-avatar",
                                [
                                  _c("v-img", {
                                    attrs: {
                                      src:
                                        "https://avataaars.io/?avatarStyle=Circle&topType=LongHairStraight&accessoriesType=Blank&hairColor=BrownDark&facialHairType=Blank&clotheType=BlazerShirt&eyeType=Default&eyebrowType=Default&mouthType=Default&skinColor=Light"
                                    }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-img",
                            {
                              attrs: {
                                src:
                                  "https://cdn.vuetifyjs.com/images/cards/forest.jpg",
                                gradient:
                                  "to top, rgba(0,0,0,.44), rgba(0,0,0,.44)"
                              }
                            },
                            [
                              _c(
                                "v-container",
                                { attrs: { "fill-height": "" } },
                                [
                                  _c(
                                    "v-layout",
                                    { attrs: { "align-center": "" } },
                                    [
                                      _c(
                                        "strong",
                                        {
                                          staticClass:
                                            "display-4 font-weight-regular mr-4 text-bold"
                                        },
                                        [_vm._v("8")]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "v-layout",
                                        {
                                          attrs: {
                                            column: "",
                                            "justify-end": ""
                                          }
                                        },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass:
                                                "headline font-weight-light"
                                            },
                                            [_vm._v("Monday")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            {
                                              staticClass:
                                                "text-uppercase font-weight-light"
                                            },
                                            [_vm._v("February 2015")]
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-card-text",
                        { staticClass: "py-0" },
                        [
                          _c(
                            "v-timeline",
                            { attrs: { "align-top": "", dense: "" } },
                            [
                              _c(
                                "v-timeline-item",
                                { attrs: { color: "pink", small: "" } },
                                [
                                  _c(
                                    "v-layout",
                                    { attrs: { "pt-3": "" } },
                                    [
                                      _c("v-flex", { attrs: { xs3: "" } }, [
                                        _c("strong", [_vm._v("5pm")])
                                      ]),
                                      _vm._v(" "),
                                      _c("v-flex", [
                                        _c("strong", [_vm._v("New Icon")]),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "caption" }, [
                                          _vm._v("Mobile App")
                                        ])
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-timeline-item",
                                {
                                  attrs: { color: "teal lighten-3", small: "" }
                                },
                                [
                                  _c(
                                    "v-layout",
                                    { attrs: { wrap: "", "pt-3": "" } },
                                    [
                                      _c("v-flex", { attrs: { xs3: "" } }, [
                                        _c("strong", [_vm._v("3-4pm")])
                                      ]),
                                      _vm._v(" "),
                                      _c("v-flex", [
                                        _c("strong", [
                                          _vm._v("Design Stand Up")
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "caption mb-2" },
                                          [_vm._v("Hangouts")]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          [
                                            _c(
                                              "v-avatar",
                                              [
                                                _c("v-img", {
                                                  attrs: {
                                                    src:
                                                      "https://avataaars.io/?avatarStyle=Circle&topType=LongHairFrida&accessoriesType=Kurt&hairColor=Red&facialHairType=BeardLight&facialHairColor=BrownDark&clotheType=GraphicShirt&clotheColor=Gray01&graphicType=Skull&eyeType=Wink&eyebrowType=RaisedExcitedNatural&mouthType=Disbelief&skinColor=Brown"
                                                  }
                                                })
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "v-avatar",
                                              [
                                                _c("v-img", {
                                                  attrs: {
                                                    src:
                                                      "https://avataaars.io/?avatarStyle=Circle&topType=ShortHairFrizzle&accessoriesType=Prescription02&hairColor=Black&facialHairType=MoustacheMagnum&facialHairColor=BrownDark&clotheType=BlazerSweater&clotheColor=Black&eyeType=Default&eyebrowType=FlatNatural&mouthType=Default&skinColor=Tanned"
                                                  }
                                                })
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "v-avatar",
                                              [
                                                _c("v-img", {
                                                  attrs: {
                                                    src:
                                                      "https://avataaars.io/?avatarStyle=Circle&topType=LongHairMiaWallace&accessoriesType=Sunglasses&hairColor=BlondeGolden&facialHairType=Blank&clotheType=BlazerSweater&eyeType=Surprised&eyebrowType=RaisedExcited&mouthType=Smile&skinColor=Pale"
                                                  }
                                                })
                                              ],
                                              1
                                            )
                                          ],
                                          1
                                        )
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-timeline-item",
                                { attrs: { color: "pink", small: "" } },
                                [
                                  _c(
                                    "v-layout",
                                    { attrs: { "pt-3": "" } },
                                    [
                                      _c("v-flex", { attrs: { xs3: "" } }, [
                                        _c("strong", [_vm._v("12pm")])
                                      ]),
                                      _vm._v(" "),
                                      _c("v-flex", [
                                        _c("strong", [_vm._v("Lunch break")])
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-timeline-item",
                                {
                                  attrs: { color: "teal lighten-3", small: "" }
                                },
                                [
                                  _c(
                                    "v-layout",
                                    { attrs: { "pt-3": "" } },
                                    [
                                      _c("v-flex", { attrs: { xs3: "" } }, [
                                        _c("strong", [_vm._v("9-11am")])
                                      ]),
                                      _vm._v(" "),
                                      _c("v-flex", [
                                        _c("strong", [
                                          _vm._v("Finish Home Screen")
                                        ]),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "caption" }, [
                                          _vm._v("Web App")
                                        ])
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5443cd40", module.exports)
  }
}

/***/ })

});