webpackJsonp([28],{

/***/ 1414:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1717)
/* template */
var __vue_template__ = __webpack_require__(1718)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/courses/CourseList.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-aa346d22", Component.options)
  } else {
    hotAPI.reload("data-v-aa346d22", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1527:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ({
  "courses": [{
    "image": "/static/img/course1.jpg",
    "type": "management",
    "popular": "top",
    "bestseller": true,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Joanna Brew",
    "oldPrice": "900",
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 5,
    "level": "All",
    "disount": 50,
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course2.jpg",
    "type": "management",
    "popular": "top",
    "bestseller": true,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/watch?v=rbTVvpHF4cU",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "John Smith Brown",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 5,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course3.jpg",
    "type": "management",
    "popular": "top",
    "bestseller": true,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Joanna Brew",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 5,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course4.jpg",
    "type": "management",
    "popular": "top",
    "bestseller": true,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Savanna South",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course5.jpg",
    "type": "management",
    "popular": "top",
    "bestseller": true,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/watch?v=LywZELVl_10",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Arthur Parker",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course3.jpg",
    "type": "design",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "John Smith Brown",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course1.jpg",
    "type": "design",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Savanna South",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course2.jpg",
    "type": "design",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/watch?v=DKN_vgJeOO8",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Arthur Parker",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "rating": 4,
    "hours": "11.5",
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course5.jpg",
    "type": "develop",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Savanna South",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "rating": 4,
    "hours": "11.5",
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course4.jpg",
    "type": "develop",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/watch?v=-4rLLoNkLEU",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Arthur Parker",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course5.jpg",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "https://www.youtube.com/watch?v=rVM3kqHGVrk",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Joanna Brew",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course3.jpg",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "John Smith Brown,Savanna South",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course4.jpg",
    "popular": "new",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Arthur Parker",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course5.jpg",
    "popular": "new",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "John Smith Brown",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course2.jpg",
    "popular": "trending",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Savanna South",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course3.jpg",
    "popular": "trending",
    "bestseller": false,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/watch?v=Y4EOOb276gY",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Arthur Parker",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course4.jpg",
    "popular": "trending",
    "bestseller": false,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/watch?v=zWtncr2UcbA",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Joanna Brew",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }],
  "instructors": [{
    "image": "/static/avatars/user-6.jpg",
    "profile": "C++",
    "place": "Iron Network",
    "name": "John Smith Brown",
    "courses": "5",
    "number": "36247",
    "type": "students"
  }, {
    "image": "/static/avatars/user-13.jpg",
    "profile": "Marketing",
    "place": "Iron Network",
    "name": "Jonathan Red",
    "courses": "11",
    "number": "7464",
    "type": "students"
  }, {
    "image": "/static/avatars/user-8.jpg",
    "profile": "Designer",
    "place": "Iron Network",
    "name": "Arthur Parker",
    "courses": "3",
    "number": "36",
    "type": "students"
  }, {
    "image": "/static/avatars/user-35.jpg",
    "profile": "Python",
    "place": "Iron Network",
    "name": "Joanna Brew",
    "courses": "15",
    "number": "150",
    "type": "students"
  }, {
    "image": "/static/avatars/user-31.jpg",
    "profile": "Angular",
    "place": "Iron Network",
    "name": "Savanna South",
    "courses": "19",
    "number": "45793",
    "type": "students"
  }],
  "courseDetail": {
    "heading": "Quisque hendrerit ex quis dui eleifend, dapibus vehicula lacus semper.",
    "content": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
    "bestSeller": "2367532",
    "createdBy": "Sahil Goyal, Girija Bansal by James Brown",
    "lastUpdates": "11/2018",
    "language": "English",
    "demoVideoUrl": "https://www.youtube.com/watch?v=GKHTSJT9kdM",
    "learn": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vestibulum at mauris semper, tristique nulla et, tristique nulla.", "Proin sit amet ex in quam vehicula maximus sit amet vel mauris.", "Proin sit amet ex in quam vehicula maximus sit amet vel mauris", "Quisque hendrerit ex quis dui eleifend, dapibus vehicula lacus semper.", "Fusce ut libero consectetur, aliquet velit sed, hendrerit sem.", "Maecenas eget urna sagittis, efficitur arcu posuere, pellentesque metus", "Donec sit amet nisi ac tellus mattis venenatis volutpat sit amet mi"],
    "description": {
      "title": "Integer pharetra mi eu libero convallis ultricies",
      "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Donec auctor sapien eget sem blandit pharetra.", "In sed tellus congue, rhoncus mi quis, iaculis magna.", "Vestibulum at mauris semper, tristique nulla et, tristique nulla.", "Vestibulum at mauris semper, tristique nulla et, tristique nulla."]
    },
    "topics": [{
      "name": "Course Overview",
      "courseDetail": [{
        "demoVideoUrl": "https://www.youtube.com/watch?v=iY6qSRdMtZQ",
        "name": "Introduction",
        "time": "11.4"
      }, {
        "demoVideoUrl": "https://www.youtube.com/watch?v=A4coW3B8V2c",
        "name": "Applications",
        "time": "11.4"
      }, {
        "demoVideoUrl": "https://www.youtube.com/watch?v=1NpisqyBoI0",
        "name": "Installing",
        "time": "11.4"
      }]
    }, {
      "name": "Basic Programming",
      "courseDetail": [{
        "demoVideoUrl": "https://www.youtube.com/watch?v=yJMtLhvDfe4",
        "name": "Welcome to Section 2",
        "time": "3.24"
      }, {
        "demoVideoUrl": "https://www.youtube.com/watch?v=D7MudYw1Fhg",
        "name": "Basics",
        "time": "7.14"
      }, {
        "demoVideoUrl": "https://www.youtube.com/watch?v=0uAPzJVDVEo",
        "name": "Solo Practice",
        "time": "5.72"
      }]
    }, {
      "name": "Advance Topics",
      "courseDetail": [{
        "demoVideoUrl": "https://www.youtube.com/watch?v=Q2AOSQmV7w4",
        "name": "Advance Programming",
        "time": "10.66"
      }, {
        "demoVideoUrl": "https://www.youtube.com/watch?v=LESBbhL1TRw",
        "name": "Difference between Topics",
        "time": "11.4"
      }, {
        "demoVideoUrl": "https://www.youtube.com/watch?v=prXvKdPeDNU",
        "name": "Wrap Up",
        "time": "7.72"
      }]
    }],
    "instructorInformation": {
      "image": "/static/avatars/user-9.jpg",
      "name": "James Colt",
      "features": [{
        "icon": "video_library",
        "feature": "Lorem ipsum dolor sit amet."
      }, {
        "icon": "book",
        "feature": "Fusce ut libero consectetur."
      }, {
        "icon": "code",
        "feature": "Proin sit amet ex in quam."
      }, {
        "icon": "phone_iphone",
        "feature": "Donec sit amet nisi ac."
      }],
      "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    },
    "billingDetails": {
      "totalPrice": 1000,
      "discountPercent": 94,
      "discountTime": "2 Day",
      "guarntee": "30",
      "includes": [{
        "icon": "video_library",
        "feature": "Lorem ipsum dolor sit amet."
      }, {
        "icon": "book",
        "feature": "Fusce ut libero consectetur."
      }, {
        "icon": "code",
        "feature": "Proin sit amet ex in quam."
      }, {
        "icon": "phone_iphone",
        "feature": "Donec sit amet nisi ac."
      }]
    },
    "moreCourses": [{
      "image": "/static/img/course1.jpg",
      "bestseller": true,
      "videoDemoStatus": false,
      "demoVideoUrl": "",
      "name": "Lorem ipsum dolor sit amet sit amesdfffffft sit amet",
      "content": "Lorem ipsum",
      "oldPrice": "900",
      "rating": 5,
      "disount": 50
    }, {
      "image": "/static/img/course2.jpg",
      "bestseller": true,
      "videoDemoStatus": false,
      "demoVideoUrl": "",
      "name": "Lorem ipsum dosdaalor sit amet sit sfddddddamet sit amet",
      "content": "Lorem ipsum",
      "oldPrice": "900",
      "disount": 50,
      "rating": 5
    }, {
      "image": "/static/img/course3.jpg",
      "bestseller": true,
      "videoDemoStatus": true,
      "demoVideoUrl": "https://www.youtube.com/watch?v=kfgOX1Uh04M",
      "name": "Loadsrem ipsum dolor sit amet sit amet sit amet",
      "content": "Lorem ipsum",
      "oldPrice": "900",
      "disount": 50,
      "rating": 5
    }]
  }
});

/***/ }),

/***/ 1535:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(1536)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/courses/CourseWidgets/CourseBanner.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-601a10f8", Component.options)
  } else {
    hotAPI.reload("data-v-601a10f8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1536:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "banner-image-wrap courses-bg-img" }, [
    _c(
      "div",
      { staticClass: "banner-content-wrap fill-height bg-warn-overlay" },
      [
        _c(
          "v-layout",
          {
            attrs: {
              "align-center": "",
              "justify-center": "",
              row: "",
              "fill-height": ""
            }
          },
          [
            _c(
              "v-flex",
              { attrs: { xs9: "", sm9: "", md10: "", lg10: "", xl10: "" } },
              [
                _c("h2", { staticClass: "white--text" }, [
                  _vm._v("Learn With Your Convenience")
                ]),
                _vm._v(" "),
                _c("h4", { staticClass: "white--text" }, [
                  _vm._v(
                    "Learn any Course anywhere anytime from our 200 courses starting from $60 USD."
                  )
                ]),
                _vm._v(" "),
                _c(
                  "v-layout",
                  { attrs: { row: "", wrap: "", "ma-0": "" } },
                  [
                    _c(
                      "v-flex",
                      {
                        attrs: {
                          xs10: "",
                          sm6: "",
                          md3: "",
                          lg2: "",
                          xl3: "",
                          "pa-0": ""
                        }
                      },
                      [
                        _c(
                          "div",
                          { staticClass: "search" },
                          [
                            _c(
                              "v-form",
                              { staticClass: "search-form" },
                              [
                                _c("v-text-field", {
                                  attrs: {
                                    dark: "",
                                    color: "white",
                                    placeholder: "Find Your Course"
                                  }
                                })
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ]
                    )
                  ],
                  1
                )
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-601a10f8", module.exports)
  }
}

/***/ }),

/***/ 1552:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1553)
/* template */
var __vue_template__ = __webpack_require__(1554)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/courses/CourseWidgets/CourseCard.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-9bd39048", Component.options)
  } else {
    hotAPI.reload("data-v-9bd39048", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1553:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__data__ = __webpack_require__(1527);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__ = __webpack_require__(56);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['data', 'colxs', 'colsm', 'colmd', 'collg', 'colxl', 'width', 'height'],
  data: function data() {
    return {
      dialog: false,
      CourseData: __WEBPACK_IMPORTED_MODULE_0__data__["a" /* default */]
    };
  },
  methods: {
    getCurrentAppLayoutHandler: function getCurrentAppLayoutHandler() {
      return Object(__WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__["b" /* getCurrentAppLayout */])(this.$router);
    }
  }
});

/***/ }),

/***/ 1554:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("v-container", [
    _c(
      "div",
      { staticClass: "course-card layout row wrap" },
      _vm._l(_vm.data, function(list, key) {
        return _c(
          "app-card",
          {
            key: key,
            attrs: {
              customClasses: "course-item-wrap",
              colClasses:
                "xs" +
                _vm.colxs +
                " sm" +
                _vm.colsm +
                " md" +
                _vm.colmd +
                " lg" +
                _vm.collg +
                " xl" +
                _vm.colxl
            }
          },
          [
            _c(
              "div",
              { staticClass: "image-wrap" },
              [
                list.videoDemoStatus == false
                  ? [
                      _c(
                        "router-link",
                        {
                          attrs: {
                            to:
                              "/" +
                              (_vm.getCurrentAppLayoutHandler() +
                                "/courses/courses-detail")
                          }
                        },
                        [
                          _c("img", {
                            attrs: { src: list.image, alt: "image" }
                          })
                        ]
                      )
                    ]
                  : _vm._e(),
                _vm._v(" "),
                list.videoDemoStatus == true
                  ? [
                      _c(
                        "router-link",
                        {
                          attrs: {
                            to:
                              "/" +
                              (_vm.getCurrentAppLayoutHandler() +
                                "/courses/courses-detail")
                          }
                        },
                        [
                          _c("img", {
                            attrs: { src: list.image, alt: "image" }
                          })
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-dialog",
                        {
                          attrs: { width: "500" },
                          model: {
                            value: _vm.dialog,
                            callback: function($$v) {
                              _vm.dialog = $$v
                            },
                            expression: "dialog"
                          }
                        },
                        [
                          _c(
                            "v-btn",
                            {
                              attrs: { slot: "activator", icon: "" },
                              slot: "activator"
                            },
                            [_c("v-icon", [_vm._v("play_circle_filled")])],
                            1
                          ),
                          _vm._v(" "),
                          _c("iframe", {
                            attrs: {
                              src: list.demoVideoUrl,
                              frameborder: "0",
                              allowfullscreen: ""
                            }
                          })
                        ],
                        1
                      )
                    ]
                  : _vm._e(),
                _vm._v(" "),
                list.bestseller == true
                  ? [
                      _c(
                        "span",
                        {
                          staticClass:
                            "best-seller bestseller-tag d-inline-block"
                        },
                        [_vm._v(_vm._s(_vm.$t("message.bestseller")))]
                      )
                    ]
                  : _vm._e()
              ],
              2
            ),
            _vm._v(" "),
            _c(
              "router-link",
              {
                attrs: {
                  to:
                    "/" +
                    (_vm.getCurrentAppLayoutHandler() +
                      "/courses/courses-detail")
                }
              },
              [
                _c("h4", { staticClass: "make-ellipse" }, [
                  _vm._v(_vm._s(list.name))
                ])
              ]
            ),
            _vm._v(" "),
            _c("span", { staticClass: "fs-12 mb-3 d-block" }, [
              _vm._v(_vm._s(list.content))
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "rating-text layout row wrap ma-0" },
              [
                _c("v-rating", {
                  attrs: {
                    value: list.rating,
                    color: "warning",
                    "background-color": "warning"
                  }
                }),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(list.rating) + " Stars")])
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "price" }, [
              _c("h4", { attrs: { color: "primary" } }, [
                _vm._v(
                  "$" + _vm._s((list.oldPrice * (100 - list.disount)) / 100)
                )
              ]),
              _vm._v(" "),
              _c("del", [_vm._v("$" + _vm._s(list.oldPrice))])
            ]),
            _vm._v(" "),
            _c(
              "app-card",
              { attrs: { customClasses: "course-hover-item" } },
              [
                _c("div", { staticClass: "header" }, [
                  _c("div", { staticClass: "meta-info" }, [
                    _c("span", { staticClass: "date-info fs-12 fw-normal" }, [
                      _vm._v("Last updated: " + _vm._s(list.lastUpdates))
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "card-header" }, [
                    _c("h4", [
                      _c("a", { attrs: { href: "#" } }, [
                        _vm._v(_vm._s(list.name))
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "meta-info mb-1" }, [
                    list.bestseller == true
                      ? _c("div", { staticClass: "mb-1" }, [
                          _c(
                            "span",
                            { staticClass: "category fs-12 fw-normal" },
                            [
                              _c("span", { staticClass: "bestseller-tag" }, [
                                _vm._v("bestseller")
                              ]),
                              _vm._v(" " + _vm._s(list.bestSell))
                            ]
                          )
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("span", { staticClass: "meta-info-block" }, [
                      _c(
                        "span",
                        { staticClass: "lectures fs-12 fw-normal" },
                        [
                          _c("v-icon", { staticClass: "cmr-8" }, [
                            _vm._v("play_circle_filled")
                          ]),
                          _vm._v(_vm._s(list.lectures) + " lectures")
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "durations fs-12 fw-normal" },
                        [
                          _c("v-icon", { staticClass: "cmr-8" }, [
                            _vm._v("access_time")
                          ]),
                          _vm._v(_vm._s(list.hours) + " hours")
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "durations fs-12 fw-normal" },
                        [
                          _c("v-icon", { staticClass: "cmr-8" }, [
                            _vm._v("show_chart")
                          ]),
                          _vm._v(_vm._s(list.level) + " Levels")
                        ],
                        1
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "short-desc" }, [
                  _c("span", [_vm._v(_vm._s(list.describe))]),
                  _vm._v(" "),
                  _c(
                    "ul",
                    { staticClass: "course-list  my-3" },
                    _vm._l(list.features, function(feature, key) {
                      return _c("li", { key: key }, [_vm._v(_vm._s(feature))])
                    }),
                    0
                  )
                ]),
                _vm._v(" "),
                _c(
                  "v-btn",
                  {
                    staticClass: "error",
                    attrs: {
                      block: "",
                      to:
                        "/" +
                        (_vm.getCurrentAppLayoutHandler() + "/courses/sign-in")
                    }
                  },
                  [_vm._v(_vm._s(_vm.$t("message.addToCart")))]
                )
              ],
              1
            )
          ],
          1
        )
      }),
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-9bd39048", module.exports)
  }
}

/***/ }),

/***/ 1576:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(1577)
/* template */
var __vue_template__ = __webpack_require__(1578)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/courses/CourseWidgets/InstructorCard.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7775a1de", Component.options)
  } else {
    hotAPI.reload("data-v-7775a1de", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1577:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__data__ = __webpack_require__(1527);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__ = __webpack_require__(56);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CourseData: __WEBPACK_IMPORTED_MODULE_0__data__["a" /* default */]
    };
  },
  methods: {
    getCurrentAppLayoutHandler: function getCurrentAppLayoutHandler() {
      return Object(__WEBPACK_IMPORTED_MODULE_1_Helpers_helpers__["b" /* getCurrentAppLayout */])(this.$router);
    }
  }
});

/***/ }),

/***/ 1578:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "instructor-card-wrap layout wrap row" },
    _vm._l(_vm.CourseData.instructors, function(instruct, key) {
      return _c(
        "app-card",
        {
          key: key,
          attrs: {
            colClasses: "xl2 lg4 md4 sm6 xs12",
            customClasses: "h-100 text-xs-center"
          }
        },
        [
          _c(
            "a",
            { staticClass: "instructor-card", attrs: { href: "#" } },
            [
              _c(
                "router-link",
                {
                  attrs: {
                    to:
                      "/" +
                      (_vm.getCurrentAppLayoutHandler() +
                        "/dashboard/ecommerce")
                  }
                },
                [
                  _c("img", {
                    attrs: {
                      src: instruct.image,
                      width: "75",
                      height: "75",
                      alt: "instructer"
                    }
                  })
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "instructor-details" }, [
                _c("span", { staticClass: "desig d-block fs-12 fw-normal" }, [
                  _vm._v(
                    _vm._s(instruct.profile) + ", " + _vm._s(instruct.place)
                  )
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "name font-weight-bold d-block" }, [
                  _vm._v(_vm._s(instruct.name))
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "instructor-counts" }, [
                _c(
                  "span",
                  { staticClass: "course-count d-block fs-12 fw-normal" },
                  [_vm._v(_vm._s(instruct.courses) + " courses")]
                ),
                _vm._v(" "),
                _c(
                  "span",
                  { staticClass: "student-count d-block fs-12 fw-normal" },
                  [
                    _c("span", { staticClass: "font-weight-bold" }, [
                      _vm._v(_vm._s(instruct.number))
                    ]),
                    _vm._v(" " + _vm._s(instruct.type))
                  ]
                )
              ])
            ],
            1
          )
        ]
      )
    }),
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7775a1de", module.exports)
  }
}

/***/ }),

/***/ 1717:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__CourseWidgets_CourseBanner__ = __webpack_require__(1535);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__CourseWidgets_CourseBanner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__CourseWidgets_CourseBanner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__CourseWidgets_InstructorCard__ = __webpack_require__(1576);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__CourseWidgets_InstructorCard___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__CourseWidgets_InstructorCard__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__CourseWidgets_CourseCard__ = __webpack_require__(1552);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__CourseWidgets_CourseCard___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__CourseWidgets_CourseCard__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__data__ = __webpack_require__(1527);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CourseData: __WEBPACK_IMPORTED_MODULE_3__data__["a" /* default */]
    };
  },
  components: {
    CourseBanner: __WEBPACK_IMPORTED_MODULE_0__CourseWidgets_CourseBanner___default.a,
    InstructorCard: __WEBPACK_IMPORTED_MODULE_1__CourseWidgets_InstructorCard___default.a,
    CourseCard: __WEBPACK_IMPORTED_MODULE_2__CourseWidgets_CourseCard___default.a
  },
  computed: {
    isTop: function isTop() {
      return this.CourseData.courses.filter(function (item) {
        return item.popular == 'top';
      });
    },
    isNew: function isNew() {
      return this.CourseData.courses.filter(function (item) {
        return item.popular == 'new';
      });
    },
    isTrending: function isTrending() {
      return this.CourseData.courses.filter(function (item) {
        return item.popular == 'trending';
      });
    }
  }
});

/***/ }),

/***/ 1718:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "courses-wrap" },
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "" } },
        [
          _c("course-banner"),
          _vm._v(" "),
          _c(
            "v-layout",
            {
              attrs: {
                row: "",
                wrap: "",
                "align-center": "",
                "justify-center": "",
                "detail-course-list": ""
              }
            },
            [
              _c(
                "v-flex",
                { attrs: { sm12: "", xs12: "", md12: "", lg12: "", xl12: "" } },
                [
                  _c(
                    "v-layout",
                    {
                      attrs: {
                        row: "",
                        wrap: "",
                        "align-start": "",
                        "fill-height": ""
                      }
                    },
                    [
                      _c(
                        "v-flex",
                        {
                          attrs: {
                            sm12: "",
                            xs12: "",
                            md12: "",
                            lg12: "",
                            xl12: ""
                          }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "popularity tab-wrap" },
                            [
                              _vm.CourseData.courses
                                ? _c(
                                    "v-tabs",
                                    {
                                      attrs: {
                                        color: "",
                                        "slider-color": "primary"
                                      }
                                    },
                                    [
                                      _vm.CourseData.courses.popular ==
                                      _vm.CourseData.courses.top
                                        ? _c(
                                            "v-tab",
                                            { attrs: { ripple: "" } },
                                            [
                                              _vm._v(
                                                "\n                              " +
                                                  _vm._s(
                                                    _vm.$t("message.top")
                                                  ) +
                                                  "\n                           "
                                              )
                                            ]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _vm.CourseData.courses.popular ==
                                      _vm.CourseData.courses.new
                                        ? _c(
                                            "v-tab",
                                            { attrs: { ripple: "" } },
                                            [
                                              _vm._v(
                                                "\n                              " +
                                                  _vm._s(
                                                    _vm.$t("message.new")
                                                  ) +
                                                  "\n                           "
                                              )
                                            ]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _vm.CourseData.courses.popular ==
                                      _vm.CourseData.courses.trending
                                        ? _c(
                                            "v-tab",
                                            { attrs: { ripple: "" } },
                                            [
                                              _vm._v(
                                                "\n                              " +
                                                  _vm._s(
                                                    _vm.$t("message.trending")
                                                  ) +
                                                  "\n                           "
                                              )
                                            ]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _vm.CourseData.courses.popular ==
                                      _vm.CourseData.courses.top
                                        ? _c(
                                            "v-tab-item",
                                            [
                                              _c("course-card", {
                                                attrs: {
                                                  data: _vm.isTop,
                                                  cols: 6,
                                                  colxl: 3,
                                                  collg: 3,
                                                  colmd: 4,
                                                  colsm: 6,
                                                  colxs: 12,
                                                  width: 305
                                                }
                                              })
                                            ],
                                            1
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _vm.CourseData.courses.popular ==
                                      _vm.CourseData.courses.new
                                        ? _c(
                                            "v-tab-item",
                                            [
                                              _c("course-card", {
                                                attrs: {
                                                  data: _vm.isNew,
                                                  cols: 6,
                                                  colxl: 3,
                                                  collg: 3,
                                                  colmd: 4,
                                                  colsm: 6,
                                                  colxs: 12,
                                                  width: 305
                                                }
                                              })
                                            ],
                                            1
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _vm.CourseData.courses.popular ==
                                      _vm.CourseData.courses.trending
                                        ? _c(
                                            "v-tab-item",
                                            [
                                              _c("course-card", {
                                                attrs: {
                                                  data: _vm.isTrending,
                                                  cols: 6,
                                                  colxl: 3,
                                                  collg: 3,
                                                  colmd: 4,
                                                  colsm: 6,
                                                  colxs: 12,
                                                  width: 305
                                                }
                                              })
                                            ],
                                            1
                                          )
                                        : _vm._e()
                                    ],
                                    1
                                  )
                                : _vm._e()
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-flex",
                        {
                          attrs: {
                            xs12: "",
                            sm12: "",
                            md10: "",
                            lg10: "",
                            xl12: "",
                            "instructor-card-wrap": ""
                          }
                        },
                        [
                          _c("div", [
                            _c("h3", [
                              _vm._v(
                                _vm._s(_vm.$t("message.popularInstructors"))
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("instructor-card")
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-aa346d22", module.exports)
  }
}

/***/ })

});