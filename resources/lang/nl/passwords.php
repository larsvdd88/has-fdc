<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Wachtwoorden moeten ten minste zes karakters hebben en overeenkomen met de bevestiging.',
    'reset' => 'Je wachtwoord is opnieuw ingesteld!',
    'sent' => 'We hebben een wachtwoord naar je e-mail gestuurd!',
    'token' => 'Dit wachtwoord-hersteltoken is ongeldig.',
    'user' => "We kunnen geen gebruikers vinden met dat e-mailadres.",

];
