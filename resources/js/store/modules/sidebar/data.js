// Sidebar Routers
export const menus = {
	'message.dashboard': {
		active: true,
		path: '/dashboard/',
	},
	'message.products': {
		active: true,
		path: '/products/',
	},
	'message.brands': {
		active: true,
		path: '/brands/',
	},
	'message.suppliers': {
		active: true,
		path: '/suppliers/',
	},
	'message.ingredients' : {
		active: true,
		path: '/ingredients/'
	},
	'message.nutrients' : {
		active: true,
		path: '/nutrients/'
	}
}
