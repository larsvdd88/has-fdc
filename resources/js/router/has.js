import HorizontalLayout from 'Container/HorizontalLayout'

// dashboard components
const HasFoodData = () => import('Views/dashboard/HasFoodData');
const Products = () => import('Views/dashboard/Products');
const Product = () => import('Views/dashboard/Products/Product');
const AddProduct = () => import('Views/dashboard/Products/AddProduct');
const Brands = () => import('Views/dashboard/Brands');
const Suppliers = () => import('Views/dashboard/Suppliers');
const Users = () => import('Views/dashboard/Users');
const Nutrients = () => import('Views/dashboard/Nutrients');
const Ingredients = () => import('Views/dashboard/Ingredients');

//courses components
const Courses = () => import('Views/courses/Courses');
const CourseList = () => import('Views/courses/CourseList');
const CoursesDetail = () => import('Views/courses/CoursesDetail');
const SignIn = () => import('Views/courses/SignIn');
const Payment = () => import('Views/courses/Payment');

// Widgets component
const ChartWidgets = () => import('Views/widgets/chart-widgets/ChartWidgets');
const UserWidgets = () => import('Views/widgets/user-widgets/UserWidgets');

//Ecommerce Widgets
const Shop = () => import('Views/ecommerce/Shop');
const Cart = () => import('Views/ecommerce/Cart');
const Checkout = () => import('Views/ecommerce/Checkout');
const CreditCard = () => import('Views/ecommerce/CreditCard');

// Inbox component
const Inbox = () => import('Views/inbox/Inbox');

// chat component
const Chat = () => import('Views/chat/Chat');

// calendar components
const Calendar = () => import('Views/calendar/Calendar');

// ui components
const Buttons = () => import('Views/ui-elements/Buttons');
const Cards = () => import('Views/ui-elements/Cards');
const Grid = () => import('Views/ui-elements/Grid');
const Groups = () => import('Views/ui-elements/Groups');
const Hover = () => import('Views/ui-elements/Hover');
const Images = () => import('Views/ui-elements/Images');
const List = () => import('Views/ui-elements/List');
const Menu = () => import('Views/ui-elements/Menu');
const Ratings = () => import('Views/ui-elements/Ratings');
const Slider = () => import('Views/ui-elements/Slider');
const Snackbar = () => import('Views/ui-elements/Snackbar');
const Tooltip = () => import('Views/ui-elements/Tooltip');
const Dialog = () => import('Views/ui-elements/Dialog');
const Select = () => import('Views/ui-elements/Select');
const Input = () => import('Views/ui-elements/Input');
const Checkbox = () => import('Views/ui-elements/Checkbox');
const Radio = () => import('Views/ui-elements/Radio');
const Toolbar = () => import('Views/ui-elements/Toolbar');
const Progress = () => import('Views/ui-elements/Progress');
const Tabs = () => import('Views/ui-elements/Tabs');
const Carousel = () => import('Views/ui-elements/Carousel');
const Chips = () => import('Views/ui-elements/Chips');
const Datepicker = () => import('Views/ui-elements/Datepicker');
const Timepicker = () => import('Views/ui-elements/Timepicker');

// chart components
const VueChartjs = () => import('Views/charts/VueChartjs');
const VueEcharts = () => import('Views/charts/VueEcharts');

// maps views
const GoogleMaps = () => import('Views/maps/GoogleMaps');
const LeafletMaps = () => import('Views/maps/LeafletMaps');
const JvectorMap = () => import('Views/maps/JvectorMap');

// Pages views
const Blank = () => import('Views/pages/Blank');
const Blog = () => import('Views/pages/Blog');
const Gallery = () => import('Views/pages/Gallery');
const Pricing1 = () => import('Views/pages/Pricing-1');
const Pricing2 = () => import('Views/pages/Pricing-2');

// users views
const UserProfile = () => import('Views/users/UserProfile');
const UsersList = () => import('Views/users/UsersList');

// drag-drop components
const Vue2Dragula = () => import('Views/drag-drop/Vue2Dragula');
const VueDraggable = () => import('Views/drag-drop/Vuedraggable');
const VueDraggableResizeable = () => import('Views/drag-drop/VueDraggableResizable');

// icons components
const Themify = () => import('Views/icons/Themify');
const Material = () => import('Views/icons/Material');

// editor components
const QuillEditor = () => import('Views/editor/QuillEditor');
const WYSIWYG = () => import('Views/editor/WYSIWYG');

// form componenets
const FormValidation = () => import('Views/forms/FormValidation');
const Stepper = () => import('Views/forms/Stepper');

// Data table componenets
const Standard = () => import('Views/tables/Standard');
const Slots = () => import('Views/tables/Slots');
const SelectableRows = () => import('Views/tables/SelectableRows');
const SearchWithText = () => import('Views/tables/SearchWithText');

// Timelines component 
const Usage = () => import('Views/timelines/Usage');
const SmallDots = () => import('Views/timelines/SmallDots');
const IconDots = () => import('Views/timelines/IconDots');
const ColoredDots = () => import('Views/timelines/ColoredDots');
const OppositeSlot = () => import('Views/timelines/OppositeSlot');
const DenseAlert = () => import('Views/timelines/DenseAlert');
const Advanced = () => import('Views/timelines/Advanced');

// Treeview component
const Treeview = () => import('Views/treeview/Treeview');

// Extensions components
const ImageCropper = () => import('Views/extensions/ImageCropper');
const VideoPlayer = () => import('Views/extensions/VideoPlayer');
const Dropzone = () => import('Views/extensions/Dropzone');

export default {
   path: '/',
   component: HorizontalLayout,
   redirect: '/dashboard/',
   children: [
      {
         path: '/dashboard/',
         component: HasFoodData,
         meta: {
            requiresAuth: true,
            title: 'message.hasFoodDataCollector',
            breadcrumb: [
              {
                breadcrumbInactive: ''
              },
              {
                breadcrumbActive: ''
              }
            ]
         }
      },
      {
         path: '/products/',
         component: Products,
         meta: {
            requiresAuth: true,
            title: 'message.productOverview',
            breadcrumb: [
              {
                breadcrumbInactive: ''
              },
              {
                breadcrumbActive: ''
              }
            ]
         }
      },
      {
         path: '/products/:id',
         component: Product,
         meta: {
            requiresAuth: true,
            title: 'message.productOverview',
            breadcrumb: [
              {
                breadcrumbInactive: 'Products'
              },
              {
                breadcrumbActive: ':id'
              }
            ]
         }
      },
      {
         path: '/products/product/add',
         component: AddProduct,
         meta: {
            requiresAuth: true,
            title: 'message.productOverview',
            breadcrumb: [
              {
                breadcrumbInactive: ''
              },
              {
                breadcrumbActive: ''
              }
            ]
         }
      },
      {
         path: '/brands/',
         component: Brands,
         meta: {
            requiresAuth: true,
            title: 'message.brands',
            breadcrumb: [
              {
                breadcrumbInactive: ''
              },
              {
                breadcrumbActive: ''
              }
            ]
         }
      },
      {
         path: '/brands/brand/add',
         component: Brands,
         meta: {
            requiresAuth: true,
            title: 'message.brands',
            breadcrumb: [
              {
                breadcrumbInactive: ''
              },
              {
                breadcrumbActive: ''
              }
            ]
         }
      },
      {
         path: '/suppliers/',
         component: Suppliers,
         meta: {
            requiresAuth: true,
            title: 'message.suppliers',
            breadcrumb: [
              {
                breadcrumbInactive: ''
              },
              {
                breadcrumbActive: ''
              }
            ]
         }
      },
      {
         path: '/suppliers/supplier/add',
         component: Suppliers,
         meta: {
            requiresAuth: true,
            title: 'message.suppliers',
            breadcrumb: [
              {
                breadcrumbInactive: ''
              },
              {
                breadcrumbActive: ''
              }
            ]
         }
      },
      {
         path: '/nutrients/',
         component: Nutrients,
         meta: {
            requiresAuth: true,
            title: 'message.nutrients',
            breadcrumb: [
              {
                breadcrumbInactive: ''
              },
              {
                breadcrumbActive: ''
              }
            ]
         }
      },
      {
         path: '/ingredients/',
         component: Ingredients,
         meta: {
            requiresAuth: true,
            title: 'message.ingredients',
            breadcrumb: [
              {
                breadcrumbInactive: ''
              },
              {
                breadcrumbActive: ''
              }
            ]
         }
      },
      {
         path: '/users/',
         component: Users,
         meta: {
            requiresAuth: true,
            title: 'message.users',
            breadcrumb: [
              {
                breadcrumbInactive: ''
              },
              {
                breadcrumbActive: ''
              }
            ]
         }
      },
   ]
}
