import en from './en';
import nl from './nl';

export default {
    nl: {
        message: nl
    },
    en: {
        message: en
    }
}