<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/products', 'ProductsController@index');
Route::post('/products', 'ProductsController@store');
Route::get('/products/{id}', 'ProductsController@show');
Route::get('/users', 'UsersController@index');
Route::get('/users/user/{id}', 'UsersController@name');
Route::get('/brands', 'BrandsController@index');
Route::post('/brands', 'BrandsController@store');
Route::get('/suppliers', 'SuppliersController@index');
Route::post('/suppliers', 'SuppliersController@store');
Route::get('/nutritionalValues', 'NutritionController@index');
Route::get('/ingredients', 'IngredientController@index');
Route::get('/ingredientNames', 'IngredientController@nameIndex');