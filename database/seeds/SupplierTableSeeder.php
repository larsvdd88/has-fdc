<?php

use Illuminate\Database\Seeder;


class SupplierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
	public function run()
    {

    	\App\Supplier::insert([
    		'name' => 'The Coca Cola Company',
    		'website' => 'https://www.coca-colacompany.com/',
    	]);

    	\App\Supplier::insert([
    		'name' => 'Lotus Bakeries',
    		'website' => 'https://lotusbakeries.nl/',
    	]);

    	\App\Supplier::insert([
    		'name' => 'Unilever',
    		'website' => 'https://www.unilever.nl',
    	]);
    }
}