<?php

use Illuminate\Database\Seeder;

class Brand
{
    public $name;
    public $supplier_id;
    public $website;
}

class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	\App\Brand::insert([
    		'name' => 'Coca Cola',
    		'supplier_id' => '1',
    		'website' => 'https://www.cocacola.nl/', 
    	]);

    	\App\Brand::insert([
    		'name' => 'Echte Enkhuizer',
    		'supplier_id' => '2',
    		'website' => 'https://www.echteenkhuizerkoek.nl/', 
    	]);

    	\App\Brand::insert([
    		'name' => 'Unox',
    		'supplier_id' => '3',
    		'website' => 'https://www.unox.nl/', 
    	]);
    }
}