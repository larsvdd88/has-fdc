<?php

use Illuminate\Database\Seeder;

class NutritionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('nutrition')->insert([
            'name' => 'energyKJ',
            'default_suffix' => 'kJ'
        ]);

        DB::table('nutrition')->insert([
    		'name' => 'eneryKCal',
            'default_suffix' => 'kCal'
	    ]);

        DB::table('nutrition')->insert([
            'name' => 'water',
            'default_suffix' => 'ml'
        ]);

        DB::table('nutrition')->insert([
            'name' => 'protein',
            'default_suffix' => 'g'
        ]);

        DB::table('nutrition')->insert([
            'name' => 'carbohydrates',
            'default_suffix' => 'g'
        ]);

        DB::table('nutrition')->insert([
            'name' => 'sugars',
            'default_suffix' => '`g'
        ]);

    }
}
