<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductNutritionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_nutrition', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('nutrition_id');
            $table->unsignedInteger('product_id');
            $table->decimal('amount');
            $table->string('unit');
            $table->timestamps();

            $table->foreign('nutrition_id')->references('id')->on('nutrition')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_nutrition');
    }
}