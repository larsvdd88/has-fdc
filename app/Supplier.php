<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    public function brands() 
    {
    	return $this->hasMany(Brand::class);
    }
}
