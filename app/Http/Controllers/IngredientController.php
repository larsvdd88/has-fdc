<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ingredient;

class IngredientController extends Controller
{
   
	public function index()
    {
    	return response()->json(Ingredient::all()->toArray());
    }

    public function nameIndex()
    {
    	return response()->json(Ingredient::all()->pluck('name')->toArray());
    }

}