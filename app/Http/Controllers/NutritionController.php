<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nutrition;

class NutritionController extends Controller
{
   
	public function index()
    {
    	return response()->json(Nutrition::all()->toArray());
    }

}
