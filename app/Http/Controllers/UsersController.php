<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{

    public function index(){
    	return response()->json(User::all()->toArray());
    }

    public function name($id){
    	$response = response()->json(User::findOrFail($id)->first_name);

    	return $response;
    }

}
