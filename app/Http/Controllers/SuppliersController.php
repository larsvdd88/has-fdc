<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;

class SuppliersController extends Controller
{
    public function index()
    {
    	return response()->json(Supplier::all()->toArray());
    }

    public function store()
    {
    	$supplier = new Supplier();

    	$supplier->name = 		request('name');
        $supplier->logo =      	request('logo');
    	$supplier->website = 	request('website');

    	$supplier->save();
    }
}