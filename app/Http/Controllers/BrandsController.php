<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;

class BrandsController extends Controller
{

    public function index()
    {

        return response()->json(Brand::with('supplier')->get()->toArray());
    }

    public function store()
    {
    	$brand = new Brand();

    	$brand->name = 		    request('name');
        $brand->logo =          request('logo');
        $brand->supplier_id=    request('supplierId');
    	$brand->website = 	    request('website');

    	$brand->save();
    }
}