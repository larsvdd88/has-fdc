<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Ingredient;
use App\Nutrition;

class ProductsController extends Controller
{
    public function index()
    {
    	return response()->json(Product::with('brand')->get()->toArray());
    }

    public function show($id)
    {
        $products = Product::with(['brand:id,name','ingredients:id,name','nutrition:id,name,amount,unit'])->get()->where('id', $id)->first();

        return response()->json($products);
    }

    public function store()
    {
    	$product = new Product();

    	$product->ean = 			   request('ean');
    	$product->name = 			   request('productName');
    	$product->brand_id = 		   request('brandId');
    	$product->added_by_id = 	   request('addedById');
    	$product->touched_by_id = 	   request('touchedById');
    	$product->pack_size = 		   request('packSize');
    	$product->portion_size = 	   request('portionSize');
        $product->portion_size_unit =  request('portionSizeUnit');

        $ingredients =                 request('ingredients');
        $nutritionalValues =           request('nutritionalValues');

    	$product->save();


        foreach ($ingredients as &$ingredient) {
            $ingredient = Ingredient::firstOrCreate(["name" => $ingredient["name"]]);
            $product->ingredients()->attach($ingredient->id); 
        }

        foreach ($nutritionalValues as &$nutritionalValue) {
            if($nutritionalValue['specified'] == true){
                $newNutritionalValue = Nutrition::firstOrCreate(["name" => $nutritionalValue['name']]);
                $product->nutrition()->attach($newNutritionalValue->id, ['amount' => $nutritionalValue['amount'], 'unit' => $nutritionalValue['defaultSuffix']]); 
            }
        }

    }
}
