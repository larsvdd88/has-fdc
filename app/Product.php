<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function ingredients()
    {
        return $this->belongsToMany('App\Ingredient', 'product_ingredients')->withTimestamps();
    }

    public function nutrition()
    {
        return $this->belongsToMany('App\Nutrition', 'product_nutrition')->withPivot('amount', 'unit')->withTimestamps();
    }
}
